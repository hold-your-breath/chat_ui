import React from 'react';
import PropTypes from 'prop-types';
import styles from './header.module.scss';

const Header = ({ messages }) => {
  const messagesCount = messages.length;

  const usersCount = () => {
    const uniqueUsers = [...new Set(messages.map((message) => message.userId))];
    return uniqueUsers.length;
  };

  const lastMessage = () => {
    const lastMesage = messages[messages.length - 1];
    return new Date(lastMesage.createdAt).toLocaleTimeString();
  };

  return (
    <div className={styles.container}>
      <div className={styles.info}>My chat</div>
      <div className={styles.info}>{usersCount()} participants</div>
      <div className={styles.info}>{messagesCount} messages</div>
      <div className={styles.lastMessage}>last message at {lastMessage()}</div>
    </div>
  );
};

Header.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Header;
