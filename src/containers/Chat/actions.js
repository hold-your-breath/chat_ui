import {
  ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LOAD_MESSAGES,
} from './actionTypes';

const loadData = (messages) => ({
  type: LOAD_MESSAGES,
  payload: {
    messages,
  },
});

export const addMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: {
    message,
  },
});

export const editMessage = (id, text) => ({
  type: EDIT_MESSAGE,
  payload: {
    id,
    text,
  },
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id,
  },
});

const fetchData = () => {
  const URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';
  return fetch(URL, { method: 'GET' }).then((result) => Promise.all([result, result.json()]));
};

export const loadMessages = () => async (dispatch) => fetchData().then(([response, json]) => {
  if (response.status === 200) {
    dispatch(loadData(json));
  }
});
