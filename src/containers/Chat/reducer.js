import {
  ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LOAD_MESSAGES,
} from './actionTypes';

export default (state = [], action) => {
  switch (action.type) {
    case LOAD_MESSAGES: {
      const { messages } = action.payload;
      return messages;
    }

    case ADD_MESSAGE: {
      const { message } = action.payload;
      return [...state, message];
    }

    case EDIT_MESSAGE: {
      const { id, text } = action.payload;
      const updatedMessage = state.map((message) => {
        if (message.id === id) {
          message.text = text;
          return message;
        }
        return message;
      });

      return updatedMessage;
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.filter((message) => message.id !== id);
      return filteredMessages;
    }

    default: return state;
  }
};
