import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MessageInput from '../MessageInput';
import MessageList from '../MessageList';
import Header from '../Header';
import styles from './chat.module.scss';
import {
  addMessage, deleteMessage, editMessage, loadMessages,
} from './actions';

const Chat = ({
  messages, addMessage: onAdd, deleteMessage: onDelete, editMessage: onEdit, loadMessages: onLoad,
}) => {
  useEffect(() => {
    onLoad();
  }, [onLoad]);

  const onAddMessage = (message) => {
    onAdd(message);
  };

  const onDeleteMessage = (id) => {
    // eslint-disable-next-line no-alert
    const isSure = window.confirm('Are you sure?');
    if (isSure) {
      onDelete(id);
    }
  };

  const onEditMessage = (id, text) => {
    onEdit(id, text);
  };

  if (!messages.length) {
    return (
      <div className={styles.chatContainer}>
        <div className={styles.spiner}>
          <div />
          <div />
          <div />
        </div>
      </div>
    );
  }
  return (
    <div className={styles.chatContainer}>
      <Header messages={messages} />
      <MessageList messages={messages} onDelete={onDeleteMessage} onEdit={onEditMessage} />
      <MessageInput onAddMessage={onAddMessage} />
    </div>
  );
};

Chat.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
  addMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  loadMessages: PropTypes.func.isRequired,
};

Chat.defaultProps = {
  messages: [],
};

const mapStateToProps = (state) => ({
  messages: state,
});

const actions = {
  addMessage,
  deleteMessage,
  editMessage,
  loadMessages,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);
