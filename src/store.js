import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import chatReducer from './containers/Chat/reducer';

const composedEnchancer = applyMiddleware(thunkMiddleware);

export default createStore(chatReducer, composedEnchancer);
