import React from 'react';
import PropTypes from 'prop-types';
import styles from './dateDivider.module.scss';

const DateDivider = ({ date }) => (
  <div className={styles.divider}>
    <span />
    <span>{date}</span>
    <span />
  </div>
);

DateDivider.propTypes = {
  date: PropTypes.string.isRequired,
};

export default DateDivider;
